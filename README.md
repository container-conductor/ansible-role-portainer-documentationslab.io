##  Deployment Documentation for Ansible Role Portainer

This section provides a detailed guide on performing a deployment of the Portainer service using Ansible. The process includes steps for updating the role, installing dependencies, and executing the playbook for deployment.

### Pre-Requisites
```
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible git -y
```

### Preparing the Environment

1. **Clone the Repository:**
   Clone the repository containing the Ansible role for Portainer to the desired location on your system.
   ```bash
   git clone https://gitlab.com/container-conductor/ansible-role-portainer.git /opt/ansible-role-portainer
  mkdir -p ~/.ansible/roles/
  sudo ln -s /opt/ansible-role-portainer ~/.ansible/roles/
   ```

2. **Navigate to the Role Directory:**
   Change to the directory where the Ansible role for Portainer is located. This is typically a directory dedicated to Ansible roles.
   ```bash
   cd /opt/ansible-role-portainer
   ```

3. **Install Role Dependencies:**
   Install any dependencies required by the role using the `ansible-galaxy` command. This step ensures that all required collections and roles are available.
   ```bash
   ansible-galaxy install -r molecule/default/requirements.yml
   ```

### Configuring the Playbook

1. **Create Playbook Directory:**
   Set up a directory to store your playbooks if it doesn't already exist.
   ```bash
   mkdir -p playbooks
   ```

2. **Create the Deployment Playbook:**
   Write a playbook specifically for deploying Portainer. This playbook will reference the Portainer role and any additional steps or roles needed post-deployment.
  *To run against local server*
   ```bash
   cat >playbooks/deploy-portainer.yml<<EOF
   - hosts: localhost
     become: true
     roles:
      - ansible-role-portainer
      - portainer-post-steps
   EOF
   ```



  *To run against remote server*
  create inventory file 
  ```bash
  sudo tee /tmp/inventory <<EOF
  ## Ansible Inventory template file used by Terraform to create an ./inventory file populated with the nodes it created

  [swarm-managers]
  ${IP_ADDRESS}

  [all:vars]
  ansible_ssh_private_key_file=/root/.ssh/id_rsa
  ansible_ssh_user=ubuntu
  ansible_ssh_common_args='-o StrictHostKeyChecking=no'
  ansible_internal_private_ip=${IP_ADDRESS}
  EOF

  cat /tmp/inventory
  ```

  create playbook file for remote server
  ```bash
  cat >playbooks/deploy-portainer.yml<<EOF
  - hosts: swarm-managers
    become: true
    roles:
    - ansible-role-portainer
    - portainer-post-steps
  EOF
  ```

### Setting Up Variables

1. **Create Variables Directory:**
   Prepare a directory for storing variable files.
   ```bash
   mkdir -p vars
   ```

2. **Configure Variables:**
   Create a YAML file to store variables such as admin credentials and registry configurations. Replace placeholders with actual values or parameterize them as needed.
   ```bash
   cat >vars/portainer.yml<<EOF
   configure_settings: false
   configure_registry: true
   admin_user: LOGIN_NAME
   admin_password: PASSWORD
   # Registry
   registry_name: REGISTRY_NAME
   registry_url: REGISTRY_URL
   registry_port: REGISTRY_PORT
   registry_auth: REGISTRY_AUTH # true or false
   # 1 (Quay.io), 2 (Azure container registry) or 3 (custom registry)
   registry_type: 3
   registry_username: REGISTRY_USERNAME
   registry_password: REGISTRY_PASSWORD
   EOF
   ```

  

### Deploying Portainer

1. **Execute the Playbook:**
   Run the playbook using the `ansible-playbook` command, specifying the inventory file and extra variables file. 
   *Local Server Deployment*
   ```bash
   ansible-playbook ./playbooks/deploy-portainer.yml --extra-vars "@vars/portainer.yml" -v
   ```
    *Remote Server Deployment*
    ```bash
    ansible-playbook -i /tmp/inventory ./playbooks/deploy-portainer.yml --extra-vars "@vars/portainer.yml" -v
    ``` 
