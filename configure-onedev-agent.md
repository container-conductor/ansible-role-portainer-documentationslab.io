# OneDev Agent Installation Script Documentation

## Overview
This script is designed to automate the installation and configuration of the OneDev agent on an Ubuntu system. It performs the following tasks:

1. Prompts the user for OneDev credentials and server IP address if the OneDev agent directory does not exist.
2. Sets the OneDev server URL.
3. Downloads the OneDev agent package.
4. Checks for the required Java and Git versions and installs them if necessary.
5. Extracts the downloaded agent package to the specified directory.
6. Configures the agent by updating the `serverUrl` in the `agent.properties` file.
7. Starts the OneDev agent.

## Prerequisites
- The script must be run on an Ubuntu system.
- The user must have sudo privileges to install necessary packages.
- The script requires `curl` to download the agent package.

## Usage
To use the script, follow these steps:

1. Save the script to a file on the target Ubuntu system, for example, `configure-onedev-agent.sh`.
2. sudo to root:
   ```bash
   sudo su
   ```
3. Download the script:
   ```bash
   wget https://gitlab.com/container-conductor/ansible-role-portainer-documentations.gitlab.io/-/raw/master/scripts/configure-onedev-agent.sh
   ```
3. Make the script executable:
   ```bash
   chmod +x configure-onedev-agent.sh
   ```
4. Run the script:
   ```bash
   ./configure-onedev-agent.sh
   ```

## Script Breakdown

### Initial Setup
The script starts by checking if the `/opt/onedev-agent/agent/` directory exists. If it does not, it prompts the user for the OneDev username, password, and server IP address.

### OneDev Server URL and Download URL
The `oneDevServerUrl` variable is set using the provided IP address. The `DOWNLOAD_URL` is also set using the IP address. These URLs are used to configure the agent and download the package.

### Credentials Encoding
The script encodes the provided username and password using Base64 for Basic Auth during the download process.

### Java and Git Installation Check
The script checks for Java 11 or higher and Git 2.11.1 or higher. If these are not installed, the script installs them using `apt`.

### Download and Extract Agent Package
If the OneDev agent directory does not exist, the script creates the specified `extractDir`, downloads the agent package using `curl` with the encoded credentials, and extracts it to the `extractDir`.

### Configuration
The script updates the `serverUrl` in the `agent.properties` file with the `oneDevServerUrl`.

### Agent Startup
Finally, the script starts the OneDev agent using `nohup` to run it in the background and redirects the output to `agent.log`.

## Error Handling
The script includes error handling to exit if any of the following occur:
- Java or Git installation fails.
- Downloading or extracting the agent package fails.
- The `agent.properties` file is not found.
- Updating the `serverUrl` in the `agent.properties` file fails.

## Logging
The output of the agent is logged to `agent.log` in the agent directory.

## Background Execution
The agent is started in the background and will continue to run after the script completes and the terminal is closed.

